# https://ru.m.wikibooks.org/wiki/SQLAlchemy

from sqlalchemy import create_engine

engine = create_engine('sqlite:///:memory:', echo=True)

# Флаг echo включает ведение лога через стандартный модуль logging Питона


#Далее мы хотим рассказать SQLAlchemy о наших таблицах. Мы начнем с одиночной таблицы users, В которой будем хранить записи о наших конечных пользователях, которые посещают некий сайт N. Мы определим нашу таблицу внутри каталога MetaData, используя конструктор Table(), который похож на SQLный CREATE TABLE:

from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
metadata = MetaData()
users_table = Table('users', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String (50)),
    Column('fullname', String),
    Column('password', String)
)

#Далее же мы пошлем базе CREATE TABLE, параметры которого будут взяты из метаданных нашей таблицы. Мы вызовем метод create_all() и передадим ему наш объект engine, который и указывает на базу. Там сначала будет проверено присутствие такой таблицы перед ее созданием, так что можно выполнять это много раз — ничего страшного не случится.

metadata.create_all(engine)

#В то время, как класс Table хранит информацию о нашей БД, он ничего не говорит о логике объектов, что используются нашим приложением. SQLAlchemy считает это отдельным вопросом. Для соответствия нашей таблице users создадим элементарный класс User. Нужно только унаследоваться от базового питоньего класса Object (то есть у нас совершенно новый класс).

class User(object):
    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password

    def __repr__(self):
        return "<User('%s','%s', '%s')>" % (self.name, self.fullname, self.password)
#__init__ — это конструктор, __repr__ же вызывается при операторе print. Они определены здесь для удобства. Они не обязательны и могут иметь любую форму. SQLAlchemy не вызывает __init__ напрямую

#Настройка отображения	Править

#Теперь мы хотим слить в едином порыве нашу таблицу user_table и класс User. Здесь нам пригодится пакет SQLAlchemy ORM. Мы применим функцию mapper, чтобы создать отображение между users_table и User.

from sqlalchemy.orm import mapper


#mapper(User, users_table) 

# <Mapper at 0x...; User>
#Функция mapper() создаст новый Mapper-объект и сохранит его для дальнейшего применения, ассоциирующегося с нашим классом. Теперь создадим и проверим объект типа User:
    
#from sqlalchemy.orm import mapper  #достать "Отобразитель" из пакета с объектно-реляционной моделью
print(mapper(User, users_table))  # и отобразить. Передает класс User и нашу таблицу
user = User("Вася", "Василий", "qweasdzxc")
print(user)  #Напечатает <User('Вася', 'Василий', 'qweasdzxc'>
#print(user.id)  #Напечатает None

#Атрибут id, который не определен в __init__, все равно существует из-за того, что колонка id существует в объекте таблицы users_table. Стандартно mapper() создает атрибуты класса для всех колонок, что есть в Table. Эти атрибуты существуют как Python дескрипторы и определяют его функциональность. Она может быть очень богатой и включать в себе возможность отслеживать изменения и АВТОМАТИЧЕСКИ подгружать данные в базу, когда это необходимо. Так как мы не сказали SQLAlchemy сохранить Василия в базу, его id выставлено на None. Когда позже мы сохраним его, в этом атрибуте будет храниться некое автоматически сформированное значение.


# 2
#Декларативное создание таблицы, класса и отображения за один раз	Править

from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///:memory:', echo=True)

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    password = Column(String)
    
    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password
    def __repr__(self):
        return "<User('%s','%s', '%s')>" % (self.name, self.fullname, self.password)

# Создание таблицы
Base.metadata.create_all(engine)

#___
#Предыдущый подход к конфигурированию, включающий таблицу Table, пользовательский класс и вызов mapper() иллюстрируют классический пример использования SQLAlchemy, в которой очень ценится разделение задач. Большое число приложений, однако, не требуют такого разделения, и для них SQLAlchemy предоставляет альтернативный, более лаконичный стиль: декларативный.

#Низлежащий объект Table, что создан нашей declarative_base() версией User, доступен через атрибут __table__

users_table = User.__table__

#Имющиеся метаданные MetaData также доступны:

metadata = Base.metadata

print (users_table)
print(metadata)

# ___
#«Ручка» базы в нашем случае — это сессия Session. Когда сначала мы запускаем приложение, на том же уровне нашего create_engine() мы определяем класс Session, что служит фабрикой объектов сессий (Session).

from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)

#В случае же, если наше приложение не имеет Engine-объекта базы данных, можно просто сделать так:

#Session = sessionmaker()
#А потом, когда вы создадите подключение к базе с помощью create_engine(), соедините его с сессией, используя configure():

#Session.configure(bind=engine)  # Как только у вас появится engine
#Такой класс Session будет создавать Session-объекты, которые привязаны к нашей базе.

#когда вам необходимо общение с базой, вы создаете объект класса Session:

#session = Session()

#Сессия здесь ассоциирована с SQLite, но у нее еще нет открытых соединений с этой базой. При первом использовании она получает соединение из набора соединений, который поддерживается engine и удерживает его до тех пор, пока мы не применим все изменения и/или не закроем объект сессии.



#	Добавление новых объектов	Править
#Чтобы сохранить наш User-объект, нужно добавить его к нашей сессии, вызвав add():

vasiaUser = User("vasia", "Vasiliy Pypkin", "vasia2000")
session = Session()
session.add(vasiaUser)

#Для примера, ниже мы создадим новый объект запроса (Query), который загружает User-объекты. Мы «отфильтровываем» по атрибуту «имя=Вася» и говорим, что нам нужен только первый результат из всего списка строк. Возвращается тот User, который равен добавленному:
ourUser = session.query(User).filter_by(name="vasia").first()

#Мы можем добавить больше User-объектов, использовав add_all()

session.add_all([User("kolia", "Cool Kolian[S.A.]","kolia$$$"), User("zina", "Zina Korzina", "zk18")])   #добавить сразу пачку записей

#А вот тут Вася решил, что его старый пароль слишком простой, так что давайте его сменим:

vasiaUser.password = "-=VP2001=-"   #старый пароль был таки ненадежен

#Сессия внимательно следит за нами. Она, для примера, знает, что Вася был модифицирован:

#session.dirty
#IdentitySet([<User('vasia','Vasiliy Pypkin', '-=VP2001=-')>])
#И что еще пара User’ов ожидают сброса в базу:

#session.new
#IdentitySet([<User('kolia','Cool Kolian[S.A.]', 'kolia$$$')>, <User('zina','Zina Korzina', 'zk18')>])
#А теперь мы скажем нашей сессии, что мы хотим отправить все оставшиеся изменения в базу и применить все изменения, зафиксировав транзакцию, которая до того была в процессе. Это делается с помощью commit():

session.commit()
#commit() сбрасывает все оставшиеся изменения в базу и фиксирует транзакции.

print(vasiaUser.id)
for instance in session.query(User).order_by(User.id): 
    print (instance.name, instance.fullname)
    
print("-" * 20)    
for name, fullname in session.query(User.name, User.fullname): 
    print (name, fullname)
    
print("-" * 20)    
for row in session.query(User, User.name).all(): 
   print (row.User, row.name)
   
print("-" * 20)       
for user in session.query(User).filter(User.name=='vasia').filter(User.fullname=='Vasiliy Pupkin'): 
   print (user)
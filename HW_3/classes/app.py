from flask import Flask, render_template, request
from sqlalchemy.orm import sessionmaker

from db import engine
from forms import AuthorFrom, JanrForm, TrackForm, session
from models import Author, Janr, Track

app = Flask(__name__, template_folder='templates')


def create_form_handler(form_class, model_class, title):
    form = form_class()
    success = False
    if request.method == 'POST':
        form = form_class(request.form)
        if form.validate():
            obj = model_class()
            form.populate_obj(obj)
            session.add(obj)
            session.commit()
            success = True

    return render_template(
        'create_tracks.html', **{
            'form': form,
            'title': title,
            'success': success
        }
    )


@app.route('/')
def home():
    tracks = session.query(Track).all()
    return render_template(
        'index.html', **{'tracks': tracks}
    )


@app.route('/create/', methods=['GET', 'POST'])
def create_track():
    return create_form_handler(TrackForm, Track, "Add Track")


@app.route('/add/author/', methods=['GET', 'POST'])
def add_author():
    return create_form_handler(AuthorFrom, Author, 'Add Author')


@app.route('/add/janr/', methods=['GET', 'POST'])
def add_janr():
    return create_form_handler(JanrForm, Janr, 'Add Janr')


if __name__ == '__main__':
    app.run(debug=True)

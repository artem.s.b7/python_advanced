from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Author(Base):
    __tablename__ = 'author'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)

    def __str__(self):
        return self.name


class Janr(Base):
    __tablename__ = 'janr'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)

    def __str__(self):
        return self.name


class Track(Base):
    __tablename__ = 'track'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    janr = Column(ForeignKey('janr.id'), nullable=False)
    author = Column(ForeignKey('author.id'), nullable=False)
    duration = Column(String, nullable=True)

    janr_obj = relationship("Janr")
    author_obj = relationship("Author")

    def __str__(self):
        return self.name



from sqlalchemy.orm import sessionmaker
from wtforms.ext.sqlalchemy.orm import model_form
from models import Track, Author, Janr

from db import engine

# session_class = sessionmaker(bind=engine)
# session = session_class()

session = sessionmaker(bind=engine)()


TrackForm = model_form(Track, db_session=session)
AuthorFrom = model_form(Author)
JanrForm = model_form(Janr)

from core.models import Group, Student, Teacher

from django.contrib import admin


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("name", "teacher")
    list_filter = ("name",)


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("last_name", "first_name", "middle_name", "age", "subject")
    list_filter = ("subject",)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ("last_name", "first_name", "middle_name", "age", 'group')
    search_fields = ("last_name",)
    list_filter = ("group",)

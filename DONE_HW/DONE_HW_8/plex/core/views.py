from core.forms import GroupForm, StudentForm, TeacherForm
from core.models import Group, Student, Teacher

from django.db.models import Avg, Count, IntegerField, Max, Min
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import DeleteView, FormView, UpdateView


class IndexView(TemplateView):
    template_name = "index.html"
    success_url = reverse_lazy("home")

    def get_context_data(self, **kwargs):
        groups = Group.objects.all().select_related().annotate(
            student_count=Count('students'),
            student_age_avg=Avg('students__age', output_field=IntegerField()),
            student_age_min=Min('students__age'),
            student_age_max=Max('students__age')
        )

        context = {
            "groups": groups
        }
        return context


class StudentView(TemplateView):
    template_name = "student_processing.html"
    success_url = reverse_lazy("home")

    def get_context_data(self, **kwargs):
        students = Student.objects.all()

        context = {
            "students": students
        }
        return context


class TeacherView(ListView):
    template_name = "teacher_processing.html"
    success_url = reverse_lazy("home")
    model = Teacher
    form_class = TeacherForm
    context_object_name = 'teachers'


class GroupView(ListView):
    template_name = "group_processing.html"
    success_url = reverse_lazy("home")
    model = Group
    form_class = TeacherForm
    context_object_name = 'groups'


class GroupDeleteView(DeleteView):
    model = Group
    template_name = 'delete_form.html'
    success_url = reverse_lazy('home')
    form_class = GroupForm


class StudentDeleteView(DeleteView):
    model = Student
    template_name = 'delete_form.html'
    success_url = reverse_lazy('home')
    form_class = StudentForm


class TeacherDeleteView(DeleteView):
    model = Teacher
    template_name = 'delete_form.html'
    success_url = reverse_lazy('home')
    form_class = TeacherForm


class GroupUpdateView(UpdateView):
    template_name = 'update_form.html'
    success_url = reverse_lazy("home")
    model = Group
    form_class = GroupForm


class StudentUpdateView(UpdateView):
    template_name = 'update_form.html'
    success_url = reverse_lazy("home")
    model = Student
    form_class = StudentForm


class TeacherUpdateView(UpdateView):
    template_name = 'update_form.html'
    success_url = reverse_lazy("home")
    model = Teacher
    form_class = TeacherForm


class GroupCreateView(FormView):
    template_name = 'create_form.html'
    form_class = GroupForm
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.save()
        return super(GroupCreateView, self).form_valid(form)


class TeacherCreateView(FormView):
    template_name = 'create_form.html'
    form_class = TeacherForm
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.save()
        return super(TeacherCreateView, self).form_valid(form)


class StudentCreateView(FormView):
    template_name = 'create_form.html'
    form_class = StudentForm
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.save()
        return super(StudentCreateView, self).form_valid(form)

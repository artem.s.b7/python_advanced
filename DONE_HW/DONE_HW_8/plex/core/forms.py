from core.fields import PhoneField
from core.models import Group, Student, Teacher

from django import forms


class GroupForm(forms.ModelForm):
    """
    Во вьюшке не удобно, там тогда в каждой
    нужно прописывать title (шанс ошибки возрастает),
    а тут оно под рукой и сразу происывается в темплите.
    Есть более оптимальный вариант?
    """
    title = "ФОРМА УЧЕТНЫХ ДАННЫХ ГРУППЫ"
    short_title = "group"

    class Meta:
        model = Group
        fields = '__all__'
        verbose_name = "Группа"
        verbose_name_plural = "Группы"


class TeacherForm(forms.ModelForm):
    title = 'ФОРМА УЧЕТНЫХ ДАННЫХ ПРЕПОДАВАТЕЛЯ'
    short_title = "teacher"

    class Meta:
        model = Teacher
        fields = '__all__'
        verbose_name = "Преподаватель"
        verbose_name_plural = "Преподаватели"


class StudentForm(forms.ModelForm):
    title = "ФОРМА УЧЕТНЫХ ДАННЫХ СТУДЕНТА"
    short_title = "student"
    phone = PhoneField()

    class Meta:
        model = Student
        fields = '__all__'
        widgets = {
            'group': forms.widgets.RadioSelect()
        }
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

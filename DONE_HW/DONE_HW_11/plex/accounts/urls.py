from django.contrib.auth import views
from django.urls import path

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('password-change/', views.PasswordChangeView.as_view(), name='password_change'), # noqa
    path('password-change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'), # noqa
    path('password-reset/', views.PasswordResetView.as_view(), name='password_reset'), # noqa
    path('password-reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'), # noqa
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'), # noqa
    path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'), # noqa
]

from core.utils import get_monobank_currency, vkurse_dp, nbu, cominvestbank, oschadbank # noqa

from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):

        get_monobank_currency()
        vkurse_dp()
        nbu()
        cominvestbank()
        oschadbank()

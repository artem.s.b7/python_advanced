from django.urls import reverse_lazy
from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"
    success_url = reverse_lazy("home")

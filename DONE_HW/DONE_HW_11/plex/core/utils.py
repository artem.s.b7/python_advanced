import datetime

from bs4 import BeautifulSoup as bs

from core.models import Currency

from django.utils import timezone

import requests

from selenium import webdriver


def currency_abb(code):
    cur = {
        980: "UAH",
        840: "USD",
        978: "EUR",
        643: "RUB"
    }
    if type(code) is int:
        return cur[code]
    return code


def get_monobank_currency():
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0' # noqa
    }
    response = requests.get('https://api.monobank.ua/bank/currency', headers=headers, verify=False) # noqa

    source_name = "MONOBANK"
    if response.status_code == 200:
        data = response.json()

        cur = {
            980: "UAH",
            840: "USD",
            978: "EUR",
            643: "RUB"
        }

        data_lst = []
        for str_dict in data:
            if str_dict["currencyCodeA"] in cur and str_dict["currencyCodeB"] == 980: # noqa
                str_dict["date"] = datetime.datetime.fromtimestamp(str_dict['date']) # noqa

                data_lst.append(Currency(
                    ccy_code=currency_abb(str_dict["currencyCodeA"]),
                    ccy_code_main=currency_abb(str_dict["currencyCodeB"]),
                    buy_rate=str_dict['rateBuy'],
                    sell_rate=str_dict['rateSell'],
                    created=str_dict["date"],
                    source_name=source_name
                ))
        Currency.objects.bulk_create(data_lst)

    else:
        print(response.status_code) # noqa


def vkurse_dp():
    url = 'http://vkurse.dp.ua/'
    response = requests.get(url)
    source_name = "vkurse.dp.ua"

    if response.status_code == 200:
        browser = webdriver.Firefox('/home/meta/Dropbox/Py/')
        browser.get(url)
        required_html = browser.page_source

        soup = bs(required_html, 'html5lib')

        ccy_abb = ["USD", "EUR", "RUB"]
        id_list_buy = ["dollarBuy", "euroBuy", "rubBuy"]
        id_list_sell = ["dollarSale", "euroSale", "rubSale"]
        ccy_save = []

        for pos in range(len(ccy_abb)):

            ccy_save.append(Currency(
                            ccy_code=ccy_abb[pos],
                            ccy_code_main="UAH",
                            buy_rate=soup.find("p", id=id_list_buy[pos]).text,
                            sell_rate=soup.find("p", id=id_list_sell[pos]).text, # noqa
                            created=datetime.datetime.now(),
                            source_name=source_name
                            )
                            )
        Currency.objects.bulk_create(ccy_save)
        browser.close()
    else:
        print(response.status_code) # noqa


def nbu():
    url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0' # noqa
    }
    response = requests.get(url=url, headers=headers, verify=False)
    source_name = "NBU"

    if response.status_code == 200:
        data = response.json()

        cur = {
            980: "UAH",
            840: "USD",
            978: "EUR",
            643: "RUB"
        }

        data_lst = []
        for str_dict in data:
            if str_dict["r030"] in cur:
                str_dict["exchangedate"] = datetime.datetime.strptime(str_dict["exchangedate"], '%d.%m.%Y') # noqa
                data_lst.append(Currency(
                    ccy_code=currency_abb(str_dict["r030"]),
                    ccy_code_main="UAH",
                    buy_rate=str_dict['rate'],
                    sell_rate=str_dict['rate'],
                    created=str_dict["exchangedate"],
                    source_name=source_name
                ))
        Currency.objects.bulk_create(data_lst)
    else:
        print(response.status_code) # noqa


def cominvestbank():
    source_name = "COMINVESTBANK"
    url = 'https://www.atcominvestbank.com/rates/json/'
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0' # noqa
    }
    response = requests.get(url, headers=headers, verify=False)

    if response.status_code == 200:
        data = response.json()
        cur = {
            980: "UAH",
            840: "USD",
            978: "EUR",
            643: "RUB"
        }

        data_lst = []
        for str_dict in data:
            if int(str_dict["iso"]) in cur:
                str_dict["currdate"] = datetime.datetime.strptime(str_dict["currdate"], '%d.%m.%Y') # noqa
                data_lst.append(Currency(
                    ccy_code=currency_abb(int(str_dict["iso"])),
                    ccy_code_main="UAH",
                    buy_rate=float(str_dict['bid']) / float(str_dict['perunit']), # noqa
                    sell_rate=float(str_dict['sale']) / float(str_dict['perunit']), # noqa
                    created=str_dict["currdate"],
                    source_name=source_name
                ))
        Currency.objects.bulk_create(data_lst)

    else:
        print(response.status_code)  # noqa


def oschadbank():
    source_name = "OSCHADBANK"
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0'  # noqa
    }
    url = "https://www.oschadbank.ua/ua"
    response = requests.get(url, headers=headers, verify=False)

    if response.status_code == 200:
        soup = bs(response.text, 'html5lib')

        ccy_abb_lst = ["USD", "EUR", "RUB"]
        buy_class_list = ["buy-USD", "buy-EUR", "buy-RUB"]
        sell_class_list = ["sell-USD", "sell-EUR", "sell-RUB"]
        data_lst = []
        for pos in range(len(ccy_abb_lst)):
            data_lst.append(Currency(
                ccy_code=ccy_abb_lst[pos],
                ccy_code_main="UAH",
                buy_rate=soup.find("strong",
                                   class_=buy_class_list[pos]).text.split()[0], # noqa
                sell_rate=soup.find("strong",
                                    class_=sell_class_list[pos]).text.split()[0], # noqa
                created=datetime.datetime.now(),
                source_name=source_name
            ))
        Currency.objects.bulk_create(data_lst)
    else:
        print(response.status_code) # noqa

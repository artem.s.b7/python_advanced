from core.models import Currency

from django.contrib import admin


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = (
        "ccy_code",
        "ccy_code_main",
        "buy_rate",
        "sell_rate",
        "created",
        "source_name",

    )

    search_fields = ("source_name", "ccy_code", "created")
    list_filter = ("ccy_code", "created")

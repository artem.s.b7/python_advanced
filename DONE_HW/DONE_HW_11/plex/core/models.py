from django.db import models


class Currency(models.Model):
    ccy_code = models.CharField(max_length=3)
    ccy_code_main = models.CharField(max_length=3)
    buy_rate = models.DecimalField(max_digits=10, decimal_places=5)
    sell_rate = models.DecimalField(max_digits=10, decimal_places=5)
    created = models.DateTimeField()
    source_name = models.CharField(max_length=255)


class Currency_code(models.Model):
    ccy_code = models.PositiveSmallIntegerField
    ccy_abb = models.CharField(max_length=3)
    ccy_name = models.CharField(max_length=20)
    ccy_sml = models.CharField(max_length=1)

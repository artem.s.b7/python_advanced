from datetime import date, timedelta

from core.models import Logger

from django.core.mail import send_mail

from plex import celery_app


@celery_app.task
def send_mail_task(subject, message, email):
    send_mail(
        subject=subject,
        message=message,
        recipient_list=[email],
        from_email="admin@examle.com"
    )


@celery_app.task
def delete_old_log():
    date_limit = date.today() - timedelta(days=7)
    Logger.objects.filter(create_date__lte=date_limit).delete()

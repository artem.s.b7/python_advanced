from core.views import ContactUsView,\
    GroupCreateView, GroupDeleteView, GroupUpdateView, GroupView,\
    IndexView,\
    StudentCreateView, StudentDeleteView, StudentUpdateView, StudentView,\
    TeacherCreateView, TeacherDeleteView, TeacherUpdateView, TeacherView


from django.urls import path

urlpatterns = [
     path('', IndexView.as_view(), name="home"),

     path('info/group', GroupView.as_view(), name="group-info"),
     path('info/teacher', TeacherView.as_view(), name="teacher-info"),
     path('info/student', StudentView.as_view(), name="student-info"),

     path('create/group', GroupCreateView.as_view(), name="group-create"),
     path('create/teacher', TeacherCreateView.as_view(), name="teacher-create"), # noqa
     path('create/student', StudentCreateView.as_view(), name="student-create"), # noqa

     path('update/group/<int:pk>', GroupUpdateView.as_view(), name="group-update"), # noqa
     path('update/teacher/<int:pk>', TeacherUpdateView.as_view(), name="teacher-update"), # noqa
     path('update/student/<int:pk>', StudentUpdateView.as_view(), name="student-update"), # noqa

     path('delete/group/<int:pk>', GroupDeleteView.as_view(), name='group-delete'), # noqa
     path('delete/teacher/<int:pk>', TeacherDeleteView.as_view(), name='teacher-delete'), # noqa
     path('delete/student/<int:pk>', StudentDeleteView.as_view(), name='student-delete'), # noqa
     path('contactform', ContactUsView.as_view(), name="contact-us-form")
]

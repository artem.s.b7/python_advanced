
from core.fields import PhoneField
from core.models import Group, Student, Teacher

from django import forms


class ContactUsForm(forms.Form):
    title = forms.CharField()
    message = forms.CharField()
    email_user = forms.EmailField()


class GroupForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = '__all__'
        verbose_name = "Группа"
        verbose_name_plural = "Группы"


class TeacherForm(forms.ModelForm):

    class Meta:
        model = Teacher
        fields = '__all__'
        verbose_name = "Преподаватель"
        verbose_name_plural = "Преподаватели"


class StudentForm(forms.ModelForm):
    phone = PhoneField()

    class Meta:
        model = Student
        fields = '__all__'
        widgets = {
            'group': forms.widgets.RadioSelect()
        }
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

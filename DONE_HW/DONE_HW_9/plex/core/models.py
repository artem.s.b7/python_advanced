from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver


# class ContactUs(models.Model):
#     title = models.CharField(max_length=255)
#     message = models.CharField(max_length=255)
#     email_user = models.EmailField()
#
#     def __str__(self):
#         return self.email_user


class Logger(models.Model):

    request_path = models.CharField(max_length=255, verbose_name="Путь")
    request_method = models.CharField(max_length=225, verbose_name="Метод")
    execution_time = models.FloatField(
        max_length=10,
        verbose_name="Время выполнения (сек)"
    )
    create_date = models.DateField(
        auto_now_add=False,
        verbose_name="Дата"
    )

    class Meta:
        verbose_name = "Логирование запросов"
        verbose_name_plural = "Логирования запросов"


class Student(models.Model):
    last_name = models.CharField(max_length=255, verbose_name="Фамилия")
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    middle_name = models.CharField(max_length=255, verbose_name="Отчество")
    age = models.PositiveSmallIntegerField(verbose_name="Возраст")
    group = models.ForeignKey("core.Group",
                              on_delete=models.CASCADE,
                              null=True, blank=True,
                              related_name="students")

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

    def __str__(self):
        data = f"{self.last_name} {self.first_name} {self.middle_name}, {self.age}, {self.group}" # noqa
        return data


class Teacher(models.Model):
    last_name = models.CharField(max_length=255, verbose_name="Фамилия")
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    middle_name = models.CharField(max_length=255, verbose_name="Отчество")
    subject = models.CharField(max_length=255,
                               blank=True,
                               null=True,
                               verbose_name="Предмет")
    age = models.PositiveSmallIntegerField(verbose_name="Возраст")

    class Meta:
        verbose_name = "Преподаватель"
        verbose_name_plural = "Преподаватели"

    def __str__(self):
        data = f"{self.last_name} {self.first_name} {self.middle_name}, {self.age}" # noqa
        return data


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    teacher = models.ForeignKey(Teacher,
                                on_delete=models.CASCADE,
                                blank=True,
                                null=True,
                                related_name='teachers')

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        return self.name


@receiver(pre_save)
def capitalize_full_name(instance, sender, **kwargs):
    models_list = ("Student", "Teacher")
    if sender.__name__ in models_list:
        instance.first_name = instance.first_name.capitalize()
        instance.last_name = instance.last_name.capitalize()
        instance.middle_name = instance.middle_name.capitalize()

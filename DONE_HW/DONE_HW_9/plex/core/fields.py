from django.core.exceptions import ValidationError

from phonenumber_field.formfields import PhoneNumberField


class PhoneField(PhoneNumberField):
    default_error_messages = {
        "invalid": "Не верный формат. Пример: +380ХХ1234590 или 0ХХ1234590"
    }

    def to_internal_value(self, data):
        phone_number = PhoneNumberField.to_python(data)
        if phone_number and not phone_number.is_valid():
            raise ValidationError(self.error_messages["invalid"])

        return phone_number.E164

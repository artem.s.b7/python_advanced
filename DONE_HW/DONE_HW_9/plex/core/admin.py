from core.models import Group, Logger, Student, Teacher

from django.contrib import admin


@admin.register(Logger)
class LoggerAdmin(admin.ModelAdmin):
    list_display = (
        "create_date",
        "request_path",
        "request_method",
        "execution_time",
    )

    search_fields = ("request_path",)
    list_filter = ("request_method", "create_date")


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("name", "teacher")
    list_filter = ("name",)


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("last_name", "first_name", "middle_name", "age", "subject")
    list_filter = ("subject",)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ("last_name", "first_name", "middle_name", "age", 'group')
    search_fields = ("last_name",)
    list_filter = ("group",)

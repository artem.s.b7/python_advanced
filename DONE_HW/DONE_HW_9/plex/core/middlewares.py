from datetime import datetime as dt
from time import time

from core.models import Logger

from django.utils.deprecation import MiddlewareMixin


class LogMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.start_time = time()

    def process_response(self, request, response):
        request.end_time = time()
        exception_list = ["/admin/", "/favicon.ico/"]
        request_short = "/".join(request.path.split("/")[:2]) + "/"
        if request_short not in exception_list:
            save_data = Logger(
                                request_path=request.path,
                                request_method=request.method,
                                execution_time=round(request.end_time - request.start_time, 5), # noqa
                                create_date=dt.now().date()
            )
            save_data.save()
        return response

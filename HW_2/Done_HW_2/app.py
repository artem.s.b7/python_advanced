from flask import Flask, render_template, request
import requests
from faker import Faker

fake = Faker()
app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/requirements')
def requirements():
    with open('static/requirements.txt', 'rt') as file:
        data = file.readlines()
    return render_template('requirements.html', data=data)


@app.route('/generate_users', methods=['POST', 'GET'])
def generate_users():
    users = {}
    if request.method == 'POST' and request.form["generate_users"].isdigit()\
            and int(request.form["generate_users"]) >= 0:
        while len(users) != int(request.form["generate_users"]):
            users.setdefault(fake.first_name(), fake.free_email())
    return render_template('generate_users.html', users=users)


@app.route('/space')
def space():
    request = requests.get('http://api.open-notify.org/astros.json')
    if request.status_code == 200:
        astro = (request.json()["number"])
    else:
        astro = "Error while getting data from API.<br>Please try again later."
    return render_template('space.html', astro=astro)


if __name__ == '__main__':
    app.run(debug=True)

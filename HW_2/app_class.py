from flask import Flask, render_template, request
import random
import string


def randstr(length):
   letters = string.ascii_lowercase + string.digits + string.punctuation
   return ''.join(random.choice(letters) for i in range(length))


app = Flask(__name__, template_folder='templates')


@app.route('/index/')
def home():
    some_var = 12
    user = {
        'name': 'loki',
        'age': 17
    }
    # with open('templates/index.html') as tpl:
    #     return tpl.read().replace(
    #         "{some_var}", str(some_var)
    #     )
    # return render_template(
    #     'sub_folder/ololo.html'
    # )
    context = {
        'some_var': some_var,
        'user': user
    }
    return render_template(
        'index.html', **context
    )


@app.route('/password/generator/', methods=['POST', 'GET'])
def pwd_generator():
    pwd = ''
    if request.method == 'POST' and request.form['pwd_len'].isdigit():
        pwd = randstr(int(request.form['pwd_len']))
        # print(randstr(request.form['pwd_len']))
    return render_template(
        'pwd_generator.html',
        pwd=pwd
    )


if __name__ == '__main__':
    app.run(debug=True)

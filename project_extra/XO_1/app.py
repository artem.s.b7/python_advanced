import numpy as np
import random

from flask import Flask, render_template, request

def init_board():
    board_matrix = 3
    board =np.array([" "] * board_matrix ** 2 )
    board = board.reshape((board_matrix,                 board_matrix))
    return board
    


def board_shema(matrix):
    board_matrix = 3
    array = []

    y_line = ([[matrix[i][j] for i in range(board_matrix)] for j in range(board_matrix)])
    x_line = ([[matrix[j][i] for i in range(board_matrix)] for j in range(board_matrix)])
    diag_forv = [([matrix[i][i] for i in range(board_matrix)])]
    diag_rev = [([matrix[board_matrix - 1 - i][i] for i in range(board_matrix - 1, -1, -1)])]
    #print(f"debug_rev: {diag_rev}")
    for val in x_line:
        array.append(val)
    for val in y_line:
        array.append(val)
    for val in diag_forv:
        array.append(val)
    for val in diag_rev:
        array.append(val)
    return np.array(array)

  
def win_validate(shema):
    print(f"---")
    board_array = shema
   # board_array =  board_shema(board)
    for index, value in enumerate(board_array):
        #print(index, value)
        #print(np.where(board_array == value))
        sum_x = (board_array[index] == "X").sum()
        sum_o = (board_array[index] == "O").sum()
        if sum_x == 3:
            return "You win"     
        if sum_o == 3:
            return "You loose"  
        if np.sum(board == " ") == 0:
            return "DRAW"
        #moves_left = np.sum(board == " ")
#        player_turn = moves_left % 2 == 0
    return "Next turn"
    
def human_move(position):
    index_1 = position // 3
    index_2 = position % 3
    board[index_1][index_2] = "X"  

def find_values():
    print(f"---")
    count = 0
    alter_var = []
    print(f"debug_ board_1: {board}")
    board_temp = board
    print(f"debug_ board: {board}")
   # print(f"debug board_temp {board_temp}")
    for i in range(len(board_temp)):
      #  print(f" debug i {i}")
        for v in range(3):
            if board_temp[i][v] not in ["X", "O"]:
                board_temp[i][v] = count
            count += 1 
  #  print(f"debug board_temp {board_temp}")
    board_array =  board_shema(board_temp)
    for index, value in enumerate(board_array):
        #print(index, value)
        #print(np.where(board_array == value))
        sum_x = (board_array[index] == "X").sum()
        sum_o = (board_array[index] == "O").sum()
        if sum_x == 3 or sum_o == 3:
            return "END"
        if sum_x + sum_o == 3:
            continue
        if sum_x == 2 or sum_o == 2:
           # print(f"sum: {sum_x}, {sum_o}")
            val = {index: value}
            #print(f"val: {val}")
            return np.array(value)
        if sum_x == 0 and sum_o <2:
            alter_var.append(value)
    if len(alter_var) != 0:
        return np.array(alter_var)
    return   board_array

def move(find_pos):
    
    #board_array =  board_shema(board)
    print(f"debug_find_97: {find_pos}")
    board_array = find_pos #
    
    print(f"array_99: {board_array}")
    optimal_find = dict(zip(*np.unique(board_array, return_counts=True)))
    optimal_find.pop("X", None)
    optimal_find.pop("O", None)
    #print(f"optimal: {optimal_find}")
    step_list = []
    if len(optimal_find.items()) == 0:
        print("end")
        return "END"
    
    for key, value in optimal_find.items():
        if max(optimal_find.values()) == value:
          #  print("test")
#            print(f"key: {key}")
#            print(f"value: {value}")
            step_list.append(key)
        #    if key not in value:
#                step_list.append(key)
   # print(f"step_list: {step_list}")
    optimal = random.choice(step_list)
    #print(f"optimal: {optimal}")
    #best = [0, 2, 6, 8]
    #print (len(np.where(board_array == optimal)[0]))
    if  len(np.where(board_array == optimal)[0]) != 0:
      #  print(f"array---: {board_array}")
     #   print(f"find: {board_array.index((6)}")
        #step = np.where(board_array == optimal)
        step = np.where(board == optimal)
        #print(f"step--: {step}")
        raw = step[0][0]
        pos = step[1][0]
        board[raw][pos] = "O"





            
       
          
# print(f"----- start -----")
# board = init_board()  # renew board
# board_shema(board)  # existing
#
#
#
# print(f"\n{board}\n")
#
# print(f"----- 1 -----")
#
# board[0][0]= "X"
# print(f"\n{board}\n")
# print(board_shema(board))
#
# print(f"----- 2 -----")
#
# #board_shema(board)
#
# board[1][1]= "O"
# print(f"\n{board}\n")
# print(board_shema(board))
#
# print(f"----- 3 -----")
# #board[0][1]= "X"
# board[0][2]= "X"
# board[1][0]= "O"
# #board[1][2]= "O"
#
#
# pos=8
# if win_validate(board_shema(board)) == "Next turn":
#     human_move(pos)
#     print(board_shema(board))
#
# if win_validate(board_shema(board)) == "Next turn":
#     move(find_values())
#     print(board_shema)
#
#
#
#
#
#
#
#
# print(f"debug__find value: {find_values()}")
# print(f"---- +++ -----")
#
#
#
#
# print(board)
# print(find_values())
# #move(find_values())
# print(board)
#
#

app = Flask(__name__, template_folder='templates')


# board_list = list(board)
# for i in board_list:
#     print(i)
# print(board_list)

"""
Steps:
1_ 
print(f"----- start -----")
board = init_board()  # renew board

2_
win_val = win_validate(board_shema(board)))
board_shema(board)  # existing 
input human move:
check for win by if
win_validate
human_move(input)


"""
board = init_board()  # renew board

@app.route('/', methods=['GET', 'POST'])
def tictactoe():

    if request.method == 'GET' and len(request.args) > 0 :
        query = request.args.keys()
        for key in query:
            print(f"debug - {key}")
            if key.isdigit:
                query = key
                human_move(int(query))
                # move(["X",1,"X"])
                move(find_values())
                print(f"Debug__237: {board}")
                board_shema(board)


    print(board)
    return render_template(
        'index.html', **{'boards': board})


# @app.route('/start/', method=['GET'])
# def new_game():
#     init_board()
#     return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)

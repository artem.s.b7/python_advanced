import timeit

code_to_test = """
data = [i for i in range(1, 30000000, 3)]
data.pop(3)

def find_num(val:list):
    total = ((val[0] + val[-1]) * (len(val) +1) / 2)
    return total - sum(data)
"""

trys = 1
elapsed_time = timeit.timeit(code_to_test, number = trys) / trys
print(f"Result: {elapsed_time}")
text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?! Placerat in egestas erat imperdiet sed euismod. Venenatis cras sed felis eget velit aliquet sagittis! Cras sed felis eget velit. Lobortis scelerisque fermentum dui faucibus in. Facilisis sed odio morbi quis commodo. Enim ut sem viverra aliquet eget sit amet. Nulla aliquet enim tortor at. Pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl? Non pulvinar neque laoreet suspendisse interdum consectetur libero... Eu facilisis sed odio morbi quis. Elit at imperdiet dui accumsan."
val = "?!."
sent = text.split()
print(f"Total words: {len(sent)}")
count = 0

for i in sent:
    for smbl in val:
        if smbl in i:
            count += 1
            break

print(f"Total sentences: {count}")

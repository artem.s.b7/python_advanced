from flask import Flask, render_template, request
from forms import BookForm, AuthorForm, GenreForm, session
from models import Book, Genre, Author


app = Flask(__name__, template_folder='templates')


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


def create_form_handler(form_class, model_class, title):
    form = form_class()
    success = False
    if request.method == 'POST':
        form = form_class(request.form)
        if form.validate():
            obj = model_class()
            form.populate_obj(obj)
            session.add(obj)
            session.commit()
            success = True

    return render_template(
        'create.html', **{
            'form': form,
            'title': title,
            'success': success
        }
    )


@app.route('/')
def listdata():
    books = session.query(Book)
    genre = dict(session.query(Genre.name, Genre.id))
    year = [year.year for year in books]
    query = request.args
    for key, value in query.items():
        if value != "ALL":
            if value in genre.keys():
                value = genre.get(value)
            books = books.filter_by(**{key: value})

    return render_template(
        'books.html', **{'books': books, 'genre': genre, 'year': year}
    )


@app.route('/create/author', methods=['GET', 'POST'])
def create_author():
    return create_form_handler(AuthorForm, Author, "Add New Author")


@app.route('/create/genre/', methods=['GET', 'POST'])
def add_genre():
    return create_form_handler(GenreForm, Genre, 'Add New Genre')


@app.route('/create/book/', methods=['GET', 'POST'])
def add_book():
    return create_form_handler(BookForm, Book, 'Add New Book')


@app.route('/update/book/<book_id>/', methods=['GET', 'POST'])
def update_book(book_id):
    id = book_id
    book = session.query(Book).filter(Book.id == id).one()
    form = BookForm(obj=book)
    print(f"DEBUG book_id: {book}")
    print(f"DEBUG form: {form}")
    success = False
    if request.method == 'POST':
        query = request.args
        print(f"-------- {query}")
        form = BookForm(request.form)
        if form.validate():
            form.populate_obj(book)
            session.add(book)
            session.commit()
            success = True
    return render_template(
        'update_book.html', **{
            'book_id': book,
            'form': form,
            'success': success})


if __name__ == '__main__':
    app.run(debug=True)

from sqlalchemy.orm import sessionmaker, scoped_session
from wtforms.ext.sqlalchemy.orm import model_form
from models import Book, Author, Genre

from db import engine


session = scoped_session(sessionmaker(bind=engine))
AuthorForm = model_form(Author)
GenreForm = model_form(Genre)

BookForm = model_form(Book, db_session=session,
                      field_args={"genre_obj": {"label": "Genre"},
                                  "author_obj": {'label': "Author"},
                                  "name": {'label': "Book title"},
                                  "year": {'label': "Year of edition"}})

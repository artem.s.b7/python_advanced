from django.contrib import admin

from core.models import Group, Student
# Register your models here.
admin.site.register(Group)
admin.site.register(Student)
def parse(query: str) -> dict:
    query_dict = {}
    query = query.replace("?", " ").replace("&", " ").split()
    for val in query:
        if "=" in val:
            val = val.split("=")
            query_dict.setdefault(val[0], val[1])
    return query_dict


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple')\
           == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&')\
           == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}

def parse_cookie(query: str) -> dict:
    query_dict = {}
    query = query.replace("?", " ").replace("&", " ").replace(";", " ").split()
    for val in query:
        if "=" in val:
            val = val[: val.find("=")] + " " + val[val.find("=") + 1:]
            val = val.split(" ")
            query_dict.setdefault(val[0], val[1])
    return query_dict


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;')\
           == {'name': 'Dima=User', 'age': '28'}

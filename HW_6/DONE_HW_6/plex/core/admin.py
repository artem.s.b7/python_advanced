from core.models import Group, Student, Teacher

from django.contrib import admin

admin.site.register(Group)
admin.site.register(Student)


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("last_name", "first_name", "middle_name", "age")

from core.views import GroupCreateView, IndexView, StudentCreateView

from django.urls import path


urlpatterns = [
    path('', IndexView.as_view(), name="home"),
    path('create/group', GroupCreateView.as_view(), name="group-create"),
    path('create/student', StudentCreateView.as_view(), name="student-create"),
]

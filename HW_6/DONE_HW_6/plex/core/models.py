from django.db import models


class Student(models.Model):
    last_name = models.CharField(max_length=255, verbose_name="Фамилия")
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    middle_name = models.CharField(max_length=255, verbose_name="Отчество")
    age = models.PositiveSmallIntegerField(verbose_name="Возраст")
    group = models.ForeignKey("core.Group",
                              on_delete=models.CASCADE,
                              null=True,
                              blank=True,
                              related_name="students"
                              )

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

    def __str__(self):
        data = f"{self.last_name} {self.first_name} {self.middle_name}, {self.age}, {self.group}" # noqa
        return data


class Teacher(models.Model):
    last_name = models.CharField(max_length=255, verbose_name="Фамилия")
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    middle_name = models.CharField(max_length=255, verbose_name="Отчество")
    age = models.PositiveSmallIntegerField(verbose_name="Возраст")

    class Meta:
        verbose_name = "Преподаватель"
        verbose_name_plural = "Преподаватели"

    def __str__(self):
        data = f"{self.last_name} {self.first_name} {self.middle_name}, {self.age}" # noqa
        return data


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    teacher = models.ForeignKey(Teacher,
                                on_delete=models.CASCADE,
                                null=True,
                                related_name='teachers'
                                )

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        data = f"{self.name}"
        return data

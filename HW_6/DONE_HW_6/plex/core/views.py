from core.models import Group, Student

from django.db.models.aggregates import Avg, Count, IntegerField,  Max, Min
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        groups = Group.objects.all().select_related('teacher').annotate(
            student_count=Count('students'),
            student_age_avg=Avg('students__age', output_field=IntegerField()),
            student_age_min=Min('students__age'),
            student_age_max=Max('students__age')
        )


        groups.select_related('student')
        context = {
            "groups": groups
        }

        return context


class StudentCreateView(CreateView):
    template_name = 'create_student.html'
    success_url = reverse_lazy('student-create')
    model = Student
    fields = '__all__'


class GroupCreateView(CreateView):
    template_name = 'create_group.html'
    success_url = reverse_lazy('group-create')
    model = Group
    fields = '__all__'
